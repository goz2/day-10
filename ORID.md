## ORID homework


	O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?
	R (Reflective): Please use one word to express your feelings about today's class.
	I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?
	D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?


- O (Objective): Learned about Extracting DTO. Learned about Containers, Microservices, and CI/CD.

- R (Reflective): Enlightened.

- I (Interpretive): Today's learnings were valuable. "Extracting DTO" involves extracting Data Transfer Objects from complex data structures, improving code maintainability. Understanding "Containers, Microservices, and CI/CD" is crucial for modern software development and deployment, ensuring portability and reliability.

- D (Decisional): I'm excited to apply Containers, Microservices, and CI/CD knowledge in future projects. Implementing containerization will make applications more portable and scalable. Adopting microservices will enhance modularity and maintenance. CI/CD pipelines will enable faster and reliable software releases, improving development processes. 
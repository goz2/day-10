package com.afs.restapi.mapper;

import com.afs.restapi.dto.CompanyRequest;
import com.afs.restapi.dto.CompanyResponse;
import com.afs.restapi.entity.Company;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;

public class CompanyMapper {
    public static CompanyResponse toCompanyResponse(Company company) {
        CompanyResponse companyResponse = new CompanyResponse();
        BeanUtils.copyProperties(company, companyResponse);
        companyResponse.setEmployeeNumber(company.getEmployees().size());
        return companyResponse;
    }

    public static Company toCompany(CompanyRequest companyRequest) {
        Company company = new Company();
        BeanUtils.copyProperties(companyRequest, company);
        return company;
    }

    public static Company toCompany(Company company, CompanyRequest companyRequest) {
        if (company.getEmployees() == null) {
            company.setEmployees(new ArrayList<>());
        }
        company.getEmployees().clear();
        company.getEmployees().addAll(companyRequest.getEmployees());

        if (companyRequest.getName() != null) {
            company.setName(companyRequest.getName());
        }
        return company;
    }
}

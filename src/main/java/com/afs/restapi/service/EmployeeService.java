package com.afs.restapi.service;

import com.afs.restapi.dto.EmployeeRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.afs.restapi.mapper.EmployeeMapper.toEmployeeResponse;
import static com.afs.restapi.mapper.EmployeeMapper.toEntity;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        return employeeRepository.findAllByStatusTrue().stream()
                .map(EmployeeMapper::toEmployeeResponse)
                .collect(Collectors.toList());
    }

    public EmployeeResponse update(int id, Employee toUpdate) {
        Employee employee = employeeRepository.findByIdAndStatusTrue(id).orElseThrow(EmployeeNotFoundException::new);
        if (toUpdate.getAge() != null) {
            employee.setAge(toUpdate.getAge());
        }
        if (toUpdate.getSalary() != null) {
            employee.setSalary(toUpdate.getSalary());
        }
        employeeRepository.save(employee);
        return toEmployeeResponse(employee);
    }

    public EmployeeResponse findById(int id) {
        return employeeRepository.findByIdAndStatusTrue(id)
                .map(EmployeeMapper::toEmployeeResponse)
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public List<EmployeeResponse> findByGender(String gender) {
        return employeeRepository.findByGenderAndStatusTrue(gender).stream()
                .map(EmployeeMapper::toEmployeeResponse)
                .collect(Collectors.toList());
    }

    public List<EmployeeResponse> findByPage(int pageNumber, int pageSize) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        return employeeRepository.findAllByStatusTrue(pageRequest).stream()
                .map(EmployeeMapper::toEmployeeResponse)
                .collect(Collectors.toList());
    }

    public EmployeeResponse insert(EmployeeRequest employeeRequest) {
        Employee employee = toEntity(employeeRequest);
        Employee savedEmployee = employeeRepository.save(employee);
        return toEmployeeResponse(savedEmployee);
    }

    public void delete(int id) {
        Employee employee = employeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        employee.setStatus(false);
        employeeRepository.save(employee);
    }
}

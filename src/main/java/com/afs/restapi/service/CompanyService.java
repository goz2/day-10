package com.afs.restapi.service;

import com.afs.restapi.dto.CompanyRequest;
import com.afs.restapi.dto.CompanyResponse;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.mapper.CompanyMapper;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.CompanyRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.afs.restapi.mapper.CompanyMapper.toCompany;
import static com.afs.restapi.mapper.CompanyMapper.toCompanyResponse;

@Service
public class CompanyService {
    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<CompanyResponse> getAll() {
        return companyRepository.findAll().stream()
                .map(CompanyMapper::toCompanyResponse)
                .collect(Collectors.toList());
    }

    public List<CompanyResponse> getAll(Integer page, Integer pageSize) {
        return companyRepository.findAll(PageRequest.of(page, pageSize)).stream()
                .map(CompanyMapper::toCompanyResponse)
                .collect(Collectors.toList());
    }

    public CompanyResponse findById(Integer companyId) {
        return companyRepository.findById(companyId).map(CompanyMapper::toCompanyResponse).orElseThrow(CompanyNotFoundException::new);
    }

    public CompanyResponse create(CompanyRequest companyRequest) {
        Company company = toCompany(companyRequest);
        Company savedCompany = companyRepository.save(company);
        return toCompanyResponse(savedCompany);
    }

    public void deleteCompany(Integer companyId) {
        Optional<Company> company = companyRepository.findById(companyId);
        company.ifPresent(companyRepository::delete);
    }

    public CompanyResponse update(Integer companyId, CompanyRequest companyRequest) {
        Company company = companyRepository.findById(companyId)
                .orElseThrow(CompanyNotFoundException::new);
        Company editCompany = companyRepository.save(toCompany(company, companyRequest));
        return toCompanyResponse(editCompany);
    }

    public List<EmployeeResponse> getEmployees(Integer companyId) {
        return companyRepository.findById(companyId)
                .orElseThrow(CompanyNotFoundException::new)
                .getEmployees().stream()
                .map(EmployeeMapper::toEmployeeResponse)
                .collect(Collectors.toList());
    }
}
